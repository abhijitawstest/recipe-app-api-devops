data "aws_ami" "amazon_linux" {
  most_recent = true
  filter { #If more or less than a single match is returned by the search, Terraform will fail
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"] #*this will retrieve the latest hardened image
  }
  owners = ["amazon"] #required field
}

resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}


######################################
#########Bastion Configuration########
######################################

resource "aws_instance" "bastion" {
  ami                  = data.aws_ami.amazon_linux.id
  instance_type        = "t2.micro" #update to this will stop and start the instance
  user_data            = file("./templates/bastion/user-data.sh")
  iam_instance_profile = aws_iam_instance_profile.bastion.name
  key_name             = var.bastion_key_name
  subnet_id            = aws_subnet.public_a.id #bastion is launched in the public subnet , can be used as jump server

  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]



  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}


###################################
###add security group to bastion###
###################################

resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound access"
  name        = "${local.prefix}-bastion"
  vpc_id      = aws_vpc.main.id

  ingress { #allow inbound access from any ip address on port 22 for ssh
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"] #can add static id address
  }

  egress { #from our server to the outside world on port 443. This will need to pull images from ECR
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress { #from our server to the outside world on port 80. This will need to pull images from ECR
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress { #allows access to prostgress database
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  tags = local.common_tags
}
