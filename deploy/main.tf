terraform {
  backend "s3" {
    bucket         = "abhijit-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate" #key in which or file  in which state file will be stored
    region         = "us-east-1"          #s3 bucket region
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"

  }

}

provider "aws" {
  version = "~> 2.54.0"
  region  = "us-east-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Envrionment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

#var.prefix will pull variable from the variable file
#terraform.workspace will output the workspace we are currently working on


data "aws_region" "current" {}
#this will give the current region we are using


