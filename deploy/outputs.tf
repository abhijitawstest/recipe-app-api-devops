output "db_host" {
  value = aws_db_instance.main.address #internal network address of the database server will be printed
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}
