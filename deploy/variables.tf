variable "prefix" {
  type        = string
  default     = "raad"
  description = "Every resource in the AWS will be added with this prefix"
}

variable "project" {
  type    = string
  default = "recipe-app-api-devops"
}

variable "contact" {
  type    = string
  default = "maintaineremail@abc.com"
}

variable "db_username" {
  type        = string
  description = "User name for the RDS prostgress database"
}


variable "db_password" {
  type        = string
  description = "Password for the RDS prostgress database"
}

variable "bastion_key_name" {
  default = "testkeypair1"
}

